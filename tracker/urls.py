from django.contrib import admin
from django.urls import path, include
from django.shortcuts import redirect


urlpatterns = [
    path("admin/", admin.site.urls),
    path("projects/", include("projects.urls")),
    path("", lambda request: redirect("/projects/"), name="home"),
    path("accounts/", include("accounts.urls")),
    path("tasks/", include("tasks.urls")),
]
