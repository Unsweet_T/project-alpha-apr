from django.db import models
from django.utils.timezone import now


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(default=now)
    due_date = models.DateTimeField(default=now)
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        "projects.Project",
        verbose_name="project",
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    assignee = models.ForeignKey(
        "auth.User",
        related_name="tasks",
        null=True,
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.name
